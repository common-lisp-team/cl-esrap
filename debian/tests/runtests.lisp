(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "esrap")
  (asdf:load-system "esrap/tests"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(let ((results (5am:run 'esrap-tests::esrap)))
  (5am:explain! results)
  (unless (5am:results-status results)
    (uiop:quit 1)))
