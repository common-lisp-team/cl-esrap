cl-esrap (20240514.gitd9d4100-1) unstable; urgency=medium

  * New upstream version 20240514.gitd9d4100
  * Add myself to Uploaders
  * Updated Standards-Version, no changes
  * Improve debian/upstream/metadata

 -- Peter Van Eynde <pvaneynd@debian.org>  Sun, 01 Dec 2024 13:38:56 +0100

cl-esrap (20211008.gitc99c33a-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.

  [ Sébastien Villemot ]
  * Remove myself from Uploaders

 -- Peter Van Eynde <pvaneynd@debian.org>  Sun, 01 Dec 2024 13:37:27 +0100

cl-esrap (20211008.gitc99c33a-1) unstable; urgency=medium

  * New upstream snapshot
  * Add debian/watch. Tracks scymtym’s fork as is now done by Quicklisp.
  * Move under Debian Common Lisp Team maintenance. Thanks to Dimitri Fontaine
    for his work on this package.
  * Bump to debhelper 13
  * Bump Standards-Version to 4.6.0
  * Remove Build-Depends on dh-lisp
  * Add Rules-Requires-Root: no
  * Update Vcs-* fields for move to salsa
  * Mark as M-A foreign
  * Switch Homepage to new upstream
  * d/copyright: reflect upstream changes
  * Simplify debian/rules
  * Add Recommends on cl-fiveam for testsuite
  * Add sbcl, cl-alexandria and texinfo to Build-Depends for documentation
  * doc-asdf-cache.patch: new patch, needed for building doc on build daemons
  * Ship generated documentation and register it to doc-base
  * drop-with-current-source-form.patch: new patch, removes dependency on
    Common Lisp package with-current-source-form
  * Add autopkgtest that runs the testsuite under sbcl

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 20 Dec 2021 15:01:41 +0100

cl-esrap (20180430-1.2) unstable; urgency=medium

  * Non-maintainer upload with patch provided by Ubuntu, thanks!

  [ Simon Chopin ]
  * d/p/function-type-no-star.patch: Fix invalid syntax, breaking since
    sbcl >= 2.1.9

 -- Christoph Berg <myon@debian.org>  Thu, 09 Dec 2021 15:48:22 +0100

cl-esrap (20180430-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 16:21:58 +0100

cl-esrap (20180430-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Wed, 16 May 2018 09:48:27 +0000

cl-esrap (20170630-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Thu, 06 Jul 2017 16:28:59 +0300

cl-esrap (20161031-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Sun, 20 Nov 2016 19:45:50 +0300

cl-esrap (20160825-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Mon, 29 Aug 2016 21:55:27 +0300

cl-esrap (20160318-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Wed, 06 Apr 2016 21:49:22 +0300

cl-esrap (20151218-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Sun, 17 Jan 2016 19:31:44 +0300

cl-esrap (20151031-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Mon, 02 Nov 2015 20:33:53 +0300

cl-esrap (20150804-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Wed, 02 Sep 2015 16:21:57 +0300

cl-esrap (20150608-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Wed, 15 Jul 2015 16:44:57 +0300

cl-esrap (20150302-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Sat, 02 May 2015 17:01:38 +0300

cl-esrap (20150113-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Wed, 14 Jan 2015 00:57:05 +0300

cl-esrap (20141217-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Thu, 18 Dec 2014 18:13:49 +0300

cl-esrap (20140826-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Mon, 15 Sep 2014 01:03:45 +0400

cl-esrap (20140713-2) unstable; urgency=medium

  * Fix debian/copyright

 -- Dimitri Fontaine <dim@tapoueh.org>  Fri, 22 Aug 2014 16:51:21 +0400

cl-esrap (20140713-1) unstable; urgency=medium

  * Quicklisp release update.

 -- Dimitri Fontaine <dim@tapoueh.org>  Mon, 04 Aug 2014 23:57:12 +0400
